#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    util = require('util'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const TEST_TIMEOUT = 40000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';
    const USERNAME = process.env.USERNAME;
    const PASSWORD = process.env.PASSWORD;
    const plugin = 'bookmark';

    let browser, app, apiKey;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(username, password, alreadyAuthenticated) {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);

        await waitForElement(By.xpath('//button[@data-l10n-id="index.newPad"]'));
        await browser.findElement(By.xpath('//button[@data-l10n-id="index.newPad"]')).click();

        await waitForElement(By.xpath('//a[@data-l10n-id="ep_guest_login"]'));
        await browser.findElement(By.xpath('//a[@data-l10n-id="ep_guest_login"]')).click();

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await waitForElement(By.id('editbar'));
    }

    async function createDocument() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.id('padname'));
        await browser.findElement(By.id('padname')).sendKeys('paddington');
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();

        await browser.wait(function () {
            return browser.getCurrentUrl().then(function (url) {
                return url === 'https://' + app.fqdn + '/p/paddington';
            });
        }, TEST_TIMEOUT);

        await browser.sleep(5000);
        await waitForElement(By.name('ace_outer'));
        await browser.switchTo().frame(browser.findElement(By.name('ace_outer')));
        await waitForElement(By.name('ace_inner'));
        await browser.switchTo().frame(browser.findElement(By.name('ace_inner')));
        await browser.findElement(By.xpath('//body')).sendKeys(Key.CONTROL + 'a' + Key.COMMAND + 'a' + Key.BACK_SPACE);
        await browser.findElement(By.xpath('//body')).sendKeys('Cloudron'); // do not navigate away too fast before ep commits the text
        await browser.sleep(5000);
    }

    async function getDocument() {
        await browser.get('https://' + app.fqdn + '/p/paddington');

        await browser.wait(function () {
            return browser.getCurrentUrl().then(function (url) {
                return url === 'https://' + app.fqdn + '/p/paddington';
            });
        }, TEST_TIMEOUT);

        await browser.sleep(10000);
        await waitForElement(By.name('ace_outer'));
        await browser.switchTo().frame(browser.findElement(By.name('ace_outer')));
        await waitForElement(By.name('ace_inner'));
        await browser.switchTo().frame(browser.findElement(By.name('ace_inner')));
        await browser.findElement(By.xpath('//span[text()="Cloudron"]'));
    }

    async function installPlugin() {
        await browser.get('https://' + app.fqdn + '/admin/plugins');
        await waitForElement(By.xpath(`//a[contains(text(), "${plugin}")]`));
        await browser.sleep(10000);

        const button = browser.findElement(By.xpath(`//tr[@class="ep_${plugin}"]//input[@class="do-install"] | //a[contains(@href, "ep_${plugin}")]/parent::td//following-sibling::td//button[contains(., "Install")]`));
        await browser.executeScript(`arguments[0].scrollIntoView(true)`, button);
        await browser.findElement(By.xpath(`//tr[@class="ep_${plugin}"]//input[@class="do-install"] | //a[contains(@href, "ep_${plugin}")]/parent::td//following-sibling::td//button[contains(., "Install")]`)).click();

        await waitForElement(By.xpath(`//tr[@class="ep_${plugin}"]//input[@class="do-uninstall"]  | //a[contains(@href, "ep_${plugin}")]/parent::td//following-sibling::td//button[contains(., "Uninstall")]`));
    }

    async function checkPlugin() {
        await browser.get('https://' + app.fqdn + '/admin/plugins');
        await browser.sleep(3000);

        await waitForElement(By.xpath(`//tr[@class="ep_${plugin}"] | //a[contains(text(), "${plugin}")]`));
        await waitForElement(By.xpath(`//tr[@class="ep_${plugin}"]//input[@class="do-uninstall"] | //a[contains(@href, "ep_${plugin}")]/parent::td//following-sibling::td//button[contains(., "Uninstall")]`));
    }

    function getApiKey() {
        const out = execSync(util.format('cloudron exec --app %s -- cat /app/data/APIKEY.txt', app.id));
        apiKey = out.toString('utf8');
        console.log('apiKey is ' + apiKey);
        expect(apiKey.length).to.be(64);
    }

    async function testApiAccess() {
        const response = await fetch('https://' + app.fqdn + '/api/1.2.15/listAllPads?apikey=' + apiKey);
        const body = await response.json()
        expect(response.status).to.be(200);
        expect(body.data.padIDs).to.contain('paddington');
    }

    async function makeUserAdmin() {
        execSync(`cloudron exec --app ${app.id} -- bash -c 'echo "{\\"users\\": {\\"${USERNAME}\\": {\\"is_admin\\": true }}}" > /app/data/settings.json'`);
        execSync(`cloudron restart --app ${app.id}`);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login', login.bind(null, USERNAME, PASSWORD, false));
    it('can create document', createDocument);
    it('can get existing document', getDocument);
    it('can be made admin', makeUserAdmin);
    it('can relogin', login.bind(null, USERNAME, PASSWORD, true));

    it('can install plugin', installPlugin);
    it('can check plugin', checkPlugin);
    it('can get api key', getApiKey);
    it('can access api', testApiAccess);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id); });

    it('can get existing document', getDocument);
    it('can check plugin', checkPlugin);
    it('can access api', testApiAccess);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', function () { execSync('cloudron restore --app ' + app.id, EXEC_ARGS); });

    it('can login', login.bind(null, USERNAME, PASSWORD, true));
    it('can get existing document', getDocument);
    it('can check plugin', checkPlugin);
    it('can access api', testApiAccess);

    it('move to different location', async function () {
        await browser.manage().deleteAllCookies();
        await browser.get('about:blank');
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        getAppInfo();
    });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, USERNAME, PASSWORD, true));
    it('can get existing document', getDocument);
    it('can check plugin', checkPlugin);
    it('can access api', testApiAccess);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync('cloudron install --appstore-id org.etherpad.cloudronapp --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, USERNAME, PASSWORD, true));
    it('can create document', createDocument);
    it('can get existing document', getDocument);
    it('can be made admin', makeUserAdmin);
    it('can relogin', login.bind(null, USERNAME, PASSWORD, true));
    it('can install plugin', installPlugin);
    it('can get api key', getApiKey);

    it('can update', function () { execSync('cloudron update --app ' + LOCATION, EXEC_ARGS); });

    it('can get existing document', getDocument);
    it('can check plugin', checkPlugin);
    it('can access api', testApiAccess);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
