# Etherpad Cloudron App

This repository contains the Cloudron app package source for [Etherpad](http://etherpad.org/).

## Installation

[![Install](https://cloudron.io/img/button32.png)](https://cloudron.io/button.html?app=org.etherpad.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.etherpad.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd etherpad-lite-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the docs are ok.

```
cd etherpad-lite-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

## Notes

Etherpad reads settings from 3 sources:

* src/node/utils/Settings.js has the defaults
* settings.json (that can be checked into the repo)
* credentials.json

