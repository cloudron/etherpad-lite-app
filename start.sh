#!/bin/bash

set -eu

echo "=========================="
echo "      Etherpad start      "
echo "=========================="

echo "=> Ensure /app/data/settings.json"
if [[ ! -f "/app/data/settings.json" ]]; then
   cat > "/app/data/settings.json" <<EOT
{
  "users": {
    "admin": {
      // 1) "password" can be replaced with "hash" if you install ep_hash_auth
      // 2) please note that if password is null, the user will not be created
      "password": "changeme1",
      "is_admin": true
    },
    "user": {
      // 1) "password" can be replaced with "hash" if you install ep_hash_auth
      // 2) please note that if password is null, the user will not be created
      "password": "changeme1",
      "is_admin": false
    }
  }
}
EOT
fi

if [[ ! -f /app/data/APIKEY.txt ]]; then
  node --eval="console.log(require('crypto').randomBytes(32).toString('hex'))" | tr -d '\n' > /app/data/APIKEY.txt
fi

cp -R /app/code /run/etherpad-lite

if [[ ! -f "/app/data/data" ]]; then
  cp -R /app/code/var /app/data/data
fi
rm -rf /run/etherpad-lite/var && \
  ln -sf /app/data/data /run/etherpad-lite/var

cp /app/pkg/settings.json.template /run/etherpad-lite/settings.json

rm -rf /run/etherpad-lite/APIKEY.txt
ln -sf /app/data/APIKEY.txt /run/etherpad-lite/APIKEY.txt

# built-in skins are symlinking here
mkdir -p /app/data/skins
ln -sf /app/pkg/skins/colibris /app/data/skins/
ln -sf /app/pkg/skins/no-skin /app/data/skins/

echo "=> Ensure folder permissions"
chown -R cloudron:cloudron /run/etherpad-lite /app/data

echo "=> Starting etherpad"
cd  /run/etherpad-lite
exec /usr/local/bin/gosu cloudron:cloudron pnpm run prod --settings /run/etherpad-lite/settings.json --credentials /app/data/settings.json
