### About

Etherpad is a real-time collaborative editor scalable to thousands of simultaneous real time users. It provides full data export capabilities, and runs on your server, under your control.

### Collaborating in really real-time

No more sending your stuff back and forth via email, just set up a pad, share the link and start collaborating!

Etherpad allows you to edit documents collaboratively in real-time, much like a live multi-player editor that runs in your browser. Write articles, press releases, to-do lists, etc. together with your friends, fellow students or colleagues, all working on the same document at the same time.

All instances provide access to all data through a well-documented API and supports import/export to many major data exchange formats.
