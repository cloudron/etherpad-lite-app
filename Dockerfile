FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

RUN apt-get update -y && apt-get install -y abiword tidy && rm -r /var/cache/apt /var/lib/apt/lists

# avoid npm warnings of readonlyfs
ENV NO_UPDATE_NOTIFIER="stopit"

# avoid error like warning https://github.com/ether/etherpad-lite/blob/develop/src/node/utils/Settings.js#L428
RUN mkdir /app/code/.git && echo -n "stable" > /app/code/.git/HEAD

ARG NODE_VERSION=20.18.0
RUN mkdir -p /usr/local/node-${NODE_VERSION} && curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

RUN npm i -g husky

# renovate: datasource=github-releases depName=ether/etherpad-lite versioning=semver extractVersion=^v(?<version>.+)$
ARG EP_VERSION=2.2.7
RUN curl -L https://github.com/ether/etherpad-lite/archive/${EP_VERSION}.tar.gz | tar -xz --strip-components 1 -f -

RUN ./bin/installDeps.sh

# can't set for installDeps.sh yet as we need dev deps
ENV NODE_ENV=production

# build admin UI https://github.com/ether/etherpad-lite/blob/2.0.1/bin/run.sh#L34
WORKDIR /app/code/admin
RUN pnpm i
RUN pnpm run build --emptyOutDir

WORKDIR /app/code
COPY settings.json.template /app/code/settings.json.template

RUN pnpm run install-plugins \
    asap \
    wrappy \
    hexoid \
    once \
    underscore \
    lodash \
    asap \
    wrappy \
    dezalgo \
    fast-deep-equal \
    require-from-string \
    punycode \
    uri-js \
    lru-cache \
    ep_align \
    ep_comments_page \
    ep_embedded_hyperlinks2 \
    ep_font_color \
    ep_headings2 \
    ep_markdown \
    ep_openid_connect \
    ep_user_displayname \
    ep_stable_authorid \
    ep_guest

# allow to place custom themes
RUN mv /app/code/src/static/skins /app/pkg/skins
RUN ln -s /app/data/skins /app/code/src/static/skins

COPY start.sh settings.json.template /app/pkg/
WORKDIR /app/code

CMD [ "/app/pkg/start.sh" ]
